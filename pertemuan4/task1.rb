def task1 (operation, a, b)
    if (operation == "addition" || operation.empty?)
        return a + b
    elsif (operation == "multiplication")
        return a * b
    elsif (operation == "division")
        return a / b
    elsif (operation == "modulus")
        return a % b
    else
        return "operation not supported"
    end
end

puts "Input Operation Type: "
operation = gets.chomp
puts "Input First Number: "
a = gets.chomp.to_i
puts "Input Last Number: "
b = gets.chomp.to_i
puts "Result: #{task1(operation, a, b)}"