def calc_avg (students)
    total = 0
    students.each_value {|value| total = total + value}
    return total / students.size
end

def above_avg (students, avg)
    above_students = []
    students.each {|key, value|
        if (value > avg)
            above_students << key
        end
    }
    return above_students
end

def convert (students)
    students.each {|key, value|
        if (value >= 80) 
            students[key] = "A"
        elsif (value >= 70 && value < 80) 
            students[key] = "B"
        elsif (value >= 60 && value < 70) 
            students[key] = "C"
        elsif (value >= 40 && value < 60) 
            students[key] = "D"
        else 
            students[key] = "E"
        end
    }
    return students
end

# students = {"Alice" => 75, "Bob" => 80, "Candra" => 100, "Dede" => 64, "Eka" => 90}
run = true
students = Hash.new
while (run)
    puts "Input Student name: (write end to stop the inputation)"
    student = gets.chomp

    if (students.has_key?(student))
        puts "Name: #{student} has existed"
        puts ""
        next
    elsif (student == "end")
        run = false
        next
    end

    puts "Input Student grade:"
    grade = gets.chomp.to_i
    students[student] = grade

    puts ""
end

puts ""
avg = calc_avg(students)
puts ("Class Average Grade : #{avg}")

above = above_avg(students, avg)
puts ("Number of Students with above-average grade : #{above.size}")
for i in above
    puts(i)
end

puts ""
convert(students)
students.each {|key, value|
    puts "#{key} with grade #{value}"
}