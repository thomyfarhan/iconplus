def task2 (names)
    for i in 0..names.length - 1
        if (names[i].length % 2 == 1 && names[i].slice(0) == 'A')
            names[i] = names[i].upcase
        elsif (names[i].slice(0) == 'T')
            names[i] = names[i].downcase
        else
            names[i] = names[i].reverse.capitalize
        end
    end
end

# name = ['Ahmad', 'Tono', 'Tini', 'Bambang', 'Agus', 'Agung']
names = []
run = true
puts "Input the Names (write end to stop the inputation)"
while (run)
    name = gets.chomp
    (name != "end") ? names << name : run = false
end

task2(names)

puts ""
puts "Result: "
for i in names
    puts(i)
end