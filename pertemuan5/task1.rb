class Shape
    def circumference
    end

    def area
    end
end

module CircleInfo
    def circle_info
        puts "Radius of the Circle = #{@radius}"
        puts "Circumference of the Circle = #{circumference}"
        puts "Area of the Circle = #{area}"
    end
end

module RectangleInfo
    def rectangle_info
        puts "Width of the Rectangle = #{@width}"
        puts "Height of the Rectangle = #{@height}"
        puts "Circumference of the Rectangle = #{circumference}"
        puts "Area of the Rectangle = #{area}"
    end
end

class Circle < Shape
    include CircleInfo

    def initialize (radius)
        @radius = radius
    end

    public
    def circumference
        (22/7) * @radius ** 2
    end

    public
    def area
        (22/7) * @radius * 2
    end

    attr_accessor :radius
end

class Rectangle < Shape
    include RectangleInfo

    def initialize (width, height)
        @width = width
        @height = height
    end

    public
    def circumference
        @width * @height
    end

    public
    def area
        2 * (@width * @height)
    end

    attr_accessor :width
    attr_accessor :height
end

run = true
while (run)
    puts "Choose your type of shape: "
    puts "1. Circle"
    puts "2. Rectangle"
    puts "3. End Program"
    shape = gets.chomp.to_i

    case shape
    when 1
        puts "Input the radius: "
        radius = gets.chomp.to_f

        circle = Circle.new(radius)
        circle.circle_info
    when 2
        puts "Input the width: "
        width = gets.chomp.to_f

        puts "Input the height: "
        height = gets.chomp.to_f

        rectangle = Rectangle.new(width, height)
        rectangle.rectangle_info
    when 3
        run = false
    else
        puts "Wrong Choice"
    end

    gets.chomp
end